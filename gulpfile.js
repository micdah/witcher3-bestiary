var gulp		= require('gulp');
var run			= require('gulp-run');
var files		= require('./gulpfile.files.js');
var scripts		= require('./gulpfile.scripts.js');
var styles		= require('./gulpfile.styles.js');
var deploy		= require('./gulpfile.deploy.js');


gulp.task('clean', ['styles-clean', 'scripts-clean', 'files-clean']);

gulp.task('build', ['clean', 'styles-build', 'scripts-build', 'files-build']);

gulp.task('watch', ['build', 'styles-watch', 'app-watch', 'files-watch']);


// DEFAULT
gulp.task('default', ['build']);


// Update typescript definitions
gulp.task('update-typescript-definitions', function() {
	run('tpm ')
});