/// <reference path="_libs.ts" />

module witcher3bestiary {
	'use strict';
	
	export interface IFilter<TIn,TOut> {
		Filter(input:TIn): TOut;
	}
	
	export interface IFilterFunctionFactory<TIn,TOut> {
		(): (arg:TIn) => TOut;
	}
	
	export class Application {
		private static _current: Application;
		private app: ng.IModule;
		
		constructor() {
			this.app = angular.module('w3module', []); 
		}
		
		public static Current(): Application {
			if (Application._current == null) {
				Application._current = new Application();
			}
			
			return Application._current;
		}
		
		/**
		 * Register service with the application
		 * 
		 * @param name The name of the service to add
		 * @param serviceConstructor Service constructor
		 */
		public RegisterService(name:string, serviceConstructor:Function):Application {
			this.app.service(name, serviceConstructor);
			
			return this;
		}
		
		/**
		 * Register filter factory with the application
		 * 
		 * @param name The name of the filter
		 * @param factory Filter factory returning filter function instances on demand
		 */
		public RegisterFilterFactory<TIn,TOut>(name:string, factory: IFilterFunctionFactory<TIn,TOut>): Application {
			this.app.filter(name, factory);
			
			return this;
		}
		
		/**
		 * Register filter with the application
		 * 
		 * @param name The name of the filter
		 * @param filter Filter instance to use each time the filter is invoked
		 */
		public RegisterFilter<TIn,TOut>(name:string, filter:IFilter<TIn,TOut>): Application {
			var factory: IFilterFunctionFactory<TIn,TOut> = () => (arg:TIn) => filter.Filter(arg);
			
			return this.RegisterFilterFactory(name, factory);
		}
		
		/**
		 * Register controller with the application
		 * 
		 * @param name The name of the controller
		 * @param controllerConstructor Controller constructor
		 */
		public RegisterController(name:string, controllerConstructor:Function): Application {
			this.app.controller(name, controllerConstructor);
			
			return this;
		}
	}
	
	// Setup bootstrap
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	})
}