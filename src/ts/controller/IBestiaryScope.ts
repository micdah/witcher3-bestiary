/// <reference path="../_libs.ts" />
/// <reference path="../model/Creature.ts" />
/// <reference path="IEventArgs.ts" />

module witcher3bestiary {
	'use strict';
	
	export interface IBestiaryScope extends ng.IScope {
		search: Creature;
		onSearchKeyUp: (event:IEventArgs) => void;
		showClearSearchLabel: boolean;
	}
}