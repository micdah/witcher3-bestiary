/// <reference path="../application.ts" />
/// <reference path="../services/IBestiaryService.ts" />
/// <reference path="../model/Creature.ts" />
/// <reference path="IBestiaryScope.ts" />
/// <reference path="IEventArgs.ts" />

module witcher3bestiary {
	'use strict';
	
	declare var $version:string;
	
	export class BestiaryController {
		private _scope: IBestiaryScope;
		private _timeout: ng.ITimeoutService;
		public creatures: Array<Creature>;
		public version: string;
		
		
		public static $inject: any[] =	[
			'$scope',
			'$timeout',
			'BestiaryService'
		];
		
		constructor(
			private $scope: IBestiaryScope,
			private $timeout: ng.ITimeoutService,
			private bestiaryService: IBestiaryService
		) {
			this._scope = $scope;
			this._timeout = $timeout;
			this.creatures = bestiaryService.get();
			this.version = $version;
			
			this.watchSearchParameter();
			this._scope.onSearchKeyUp = (event:IEventArgs) => BestiaryController.onSearchKeyUp(this, event);
			this._scope.showClearSearchLabel = false;
		}
		
		private watchSearchParameter(): void {
			this._scope.$watch('search.name', 
				(newValue: string, oldValue: any, scope: IBestiaryScope) => {
					this._timeout(() => {			
						// Re-initialize any tooltip
						$('[data-toggle="tooltip"]').tooltip();
					});
					
					this._scope.showClearSearchLabel = newValue != null && newValue.length > 0;
				});
		}
		
		private static onSearchKeyUp(ctrl:BestiaryController, event:IEventArgs) {
			if (event.keyCode === 27) {
				ctrl.clearSearch();
			}
		}
		
		private clearSearch(): void {
			this._scope.search.name = '';
		}
	}
	
	Application.Current().RegisterController('BestiaryController', BestiaryController);
}