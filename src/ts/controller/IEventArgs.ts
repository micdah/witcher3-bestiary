/// <reference path="../_libs.ts" />

module witcher3bestiary {
	'use strict';
	
	export interface IEventArgs {
		altKey: boolean;
		bubbles: boolean;
		cancelable: boolean;
		char: any;
		charCode: number;
		ctrlKey: boolean;
		currentTarget: any;
		data: any;
		delegateTarget: any;
		eventPhase: any;
		handleObj: any;
		isDefaultPrevented: any;
		key: any;
		keyCode: number;
		metaKey: boolean;
		originalEvent: any;
		relatedTarget: any;
		shiftKey: boolean;
		target: any;
		timeStamp: number;
		type: string;
		view: any;
		which: number;
	}
}