/// <reference path="../application.ts"/>
/// <reference path="../model/Creature.ts" />
/// <reference path="../repository/Creatures.ts" />
/// <reference path="IBestiaryService.ts"/>

module witcher3bestiary {
	'use strict';
	
	export class BestiaryService implements IBestiaryService {
		public get(): Array<Creature> {
			return Creatures.All;
		}
	}
	
	Application.Current().RegisterService('BestiaryService', BestiaryService);
}