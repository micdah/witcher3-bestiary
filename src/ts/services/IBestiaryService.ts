/// <reference path="../application.ts" />
/// <reference path="../model/Creature.ts"/>

module witcher3bestiary {
	'use strict';
	
	export interface IBestiaryService {
		get(): Array<Creature>;
	}
}