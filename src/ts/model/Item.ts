module witcher3bestiary {
	'use strict';
	
	export class Item {
		public name: string;
		public image: string;
			
		constructor(name: string, image: string) {
			this.name = name;
			if (!image) {
				this.image = 'icon-missing';
			} else {
				this.image = 'icon-' + image;
			}
		}
	}
}