module witcher3bestiary {
	'use strict';
	
	export enum CreatureClass {
		Necrhopage,
		Specter,
		Beast,
		CursedOne,
		Hybrid,
		Insectoid,
		Elementa,
		Relict,
		Ogroid,
		Draconid,
		Vampire
	}
}