/// <reference path="../Creature.ts"/>
/// <reference path="../Item.ts"/>
/// <reference path="../CreatureClass.ts"/>

module witcher3bestiary {
	export class Elementa extends Creature {
		constructor(name: string, image:string, weaknesses: Array<Item>, isBoss: boolean = false) {
			if (!image) {
				super(name, null, CreatureClass.Elementa, weaknesses, isBoss);
			} else {
				super(name, 'elementa-' + image, CreatureClass.Elementa, weaknesses, isBoss);
			}
		}
		
		public Boss(): Elementa {
			super.Boss();
			return this;
		}
	}
}