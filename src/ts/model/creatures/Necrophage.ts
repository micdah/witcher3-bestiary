/// <reference path="../Creature.ts"/>
/// <reference path="../Item.ts"/>
/// <reference path="../CreatureClass.ts"/>

module witcher3bestiary {
	export class Necrophage extends Creature {
		constructor(name: string, image:string, weaknesses: Array<Item>, isBoss: boolean = false) {
			if (!image) {
				super(name, null, CreatureClass.Necrhopage, weaknesses, isBoss);
			} else {
				super(name, 'necrophage-' + image, CreatureClass.Necrhopage, weaknesses, isBoss);
			}
		}
		
		public Boss(): Necrophage {
			super.Boss();
			return this;
		}
	}
}