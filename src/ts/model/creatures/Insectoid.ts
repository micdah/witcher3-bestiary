/// <reference path="../Creature.ts"/>
/// <reference path="../Item.ts"/>
/// <reference path="../CreatureClass.ts"/>

module witcher3bestiary {
	export class Insectoid extends Creature {
		constructor(name: string, image:string, weaknesses: Array<Item>, isBoss: boolean = false) {
			if (!image) {
				super(name, null, CreatureClass.Insectoid, weaknesses, isBoss);
			} else {
				super(name, 'insectoid-' + image, CreatureClass.Insectoid, weaknesses, isBoss);
			}
		}
		
		public Boss(): Insectoid {
			super.Boss();
			return this;
		}
	}
}