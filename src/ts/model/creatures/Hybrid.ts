/// <reference path="../Creature.ts"/>
/// <reference path="../Item.ts"/>
/// <reference path="../CreatureClass.ts"/>

module witcher3bestiary {
	export class Hybrid extends Creature {
		constructor(name: string, image:string, weaknesses: Array<Item>, isBoss: boolean = false) {
			if (!image) {
				super(name, null, CreatureClass.Hybrid, weaknesses, isBoss);
			} else {
				super(name, 'hybrid-' + image, CreatureClass.Hybrid, weaknesses, isBoss);
			}
		}
		
		public Boss(): Hybrid {
			super.Boss();
			return this;
		}
	}
}