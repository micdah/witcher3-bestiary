/// <reference path="../Creature.ts"/>
/// <reference path="../Item.ts"/>
/// <reference path="../CreatureClass.ts"/>

module witcher3bestiary {
	export class Draconid extends Creature {
		constructor(name: string, image:string, weaknesses: Array<Item>, isBoss: boolean = false) {
			if (!image) {
				super(name, null, CreatureClass.Draconid, weaknesses, isBoss);
			} else {
				super(name, 'draconid-' + image, CreatureClass.Draconid, weaknesses, isBoss);
			}
		}
		
		public Boss(): Draconid {
			super.Boss();
			return this;
		}
	}
}