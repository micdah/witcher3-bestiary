/// <reference path="../Creature.ts"/>
/// <reference path="../Item.ts"/>
/// <reference path="../CreatureClass.ts"/>

module witcher3bestiary {
	export class Vampire extends Creature {
		constructor(name: string, image:string, weaknesses: Array<Item>, isBoss: boolean = false) {
			if (!image) {
				super(name, null, CreatureClass.Vampire, weaknesses, isBoss);
			} else {
				super(name, 'vampire-' + image, CreatureClass.Vampire, weaknesses, isBoss);
			}
		}
		
		public Boss(): Vampire {
			super.Boss();
			return this;
		}
	}
}