/// <reference path="../Creature.ts"/>
/// <reference path="../Item.ts"/>
/// <reference path="../CreatureClass.ts"/>

module witcher3bestiary {
	export class Relict extends Creature {
		constructor(name: string, image:string, weaknesses: Array<Item>, isBoss: boolean = false) {
			if (!image) {
				super(name, null, CreatureClass.Relict, weaknesses, isBoss);
			} else {
				super(name, 'relict-' + image, CreatureClass.Relict, weaknesses, isBoss);
			}
		}
		
		public Boss(): Relict {
			super.Boss();
			return this;
		}
	}
}