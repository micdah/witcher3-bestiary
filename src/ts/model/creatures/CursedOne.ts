/// <reference path="../Creature.ts"/>
/// <reference path="../Item.ts"/>
/// <reference path="../CreatureClass.ts"/>

module witcher3bestiary {
	export class CursedOne extends Creature {
		constructor(name: string, image:string, weaknesses: Array<Item>, isBoss: boolean = false) {
			if (!image) {
				super(name, null, CreatureClass.CursedOne, weaknesses, isBoss);
			} else {
				super(name, 'cursed_one-' + image, CreatureClass.CursedOne, weaknesses, isBoss);
			}
		}
		
		public Boss(): CursedOne {
			super.Boss();
			return this;
		}
	}
}