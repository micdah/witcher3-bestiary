/// <reference path="../Creature.ts"/>
/// <reference path="../Item.ts"/>
/// <reference path="../CreatureClass.ts"/>

module witcher3bestiary {
	export class Beast extends Creature {
		constructor(name: string, image:string, weaknesses: Array<Item>, isBoss: boolean = false) {
			if (!image) {
				super(name, null, CreatureClass.Beast, weaknesses, isBoss);
			} else {
				super(name, 'beast-' + image, CreatureClass.Beast, weaknesses, isBoss);
			}
		}
		
		public Boss(): Beast {
			super.Boss();
			return this;
		}
	}
}