/// <reference path="../Creature.ts"/>
/// <reference path="../Item.ts"/>
/// <reference path="../CreatureClass.ts"/>

module witcher3bestiary {
	export class Ogroid extends Creature {
		constructor(name: string, image:string, weaknesses: Array<Item>, isBoss: boolean = false) {
			if (!image) {
				super(name, null, CreatureClass.Ogroid, weaknesses, isBoss);
			} else {
				super(name, 'ogroid-' + image, CreatureClass.Ogroid, weaknesses, isBoss);
			}
		}
		
		public Boss(): Ogroid {
			super.Boss();
			return this;
		}
	}
}