/// <reference path="../Creature.ts"/>
/// <reference path="../Item.ts"/>
/// <reference path="../CreatureClass.ts"/>

module witcher3bestiary {
	export class Specter extends Creature {
		constructor(name: string, image:string, weaknesses: Array<Item>, isBoss: boolean = false) {
			if (!image) {
				super(name, null, CreatureClass.Specter, weaknesses, isBoss);
			} else {
				super(name, 'specter-' + image, CreatureClass.Specter, weaknesses, isBoss);
			}
		}
		
		public Boss(): Specter {
			super.Boss();
			return this;
		}
	}
}