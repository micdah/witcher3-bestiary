/// <reference path="Item.ts"/>

module witcher3bestiary {
	'use strict';
	
	export class Potion extends Item {
		constructor(name: string, image: string) {
			if (!image) {
				super(name, null);
			} else {
				super(name, 'potion-' + image);
			}
		}
	}
}