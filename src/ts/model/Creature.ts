/// <reference path="Item.ts"/>
/// <reference path="CreatureClass.ts"/>

module witcher3bestiary {
	'use strict';
	
	export class Creature {
		public name: string;
		public creatureClass: CreatureClass;
		public weaknesses: Array<Item>;
		public image: string;
		public isBoss: boolean;
		
		constructor(name: string, image: string, creatureClass: CreatureClass, weaknesses: Array<Item>, isBoss: boolean = false) {
			this.name = name;
			if (!image) {
				this.image = 'icon-missing'
			} else {
				this.image = 'icon-creature-' + image;
			}
			this.creatureClass = creatureClass;
			this.weaknesses = weaknesses;
			this.isBoss = isBoss;
		}
		
		public Boss(): Creature {
			this.isBoss = true;
			return this;
		}
	}
}