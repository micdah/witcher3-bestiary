/// <reference path="Item.ts"/>

module witcher3bestiary {
	'use strict';
	
	export class Sign extends Item
	{
		constructor(name: string) {
			super(name, 'sign-' + name.toLowerCase() + '');
		}
	}
}