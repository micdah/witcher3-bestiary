/// <reference path="Item.ts"/>

module witcher3bestiary {
	'use strict';
	
	export class Bomb extends Item {
		constructor(name: string, image:string) {
			if (!image) {
				super(name, null);
			} else {
				super(name, 'bomb-' + image);
			}
		}
	}
}