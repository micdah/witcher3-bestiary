/// <reference path="../application.ts" />
/// <reference path="../model/CreatureClass.ts" />

module witcher3bestiary {
	export class CreatureClassDisplayFilter implements IFilter<CreatureClass,string> {
		public Filter(creatureClass: CreatureClass): string {
			return CreatureClassDisplayFilter.Render(creatureClass);
		}
		
		public static Render(creatureClass: CreatureClass): string {
			switch (creatureClass) {
				case CreatureClass.Necrhopage:
					return "Necrophage";
				case CreatureClass.Specter:
					return "Specter";
				case CreatureClass.Beast:
					return "Beast";
				case CreatureClass.CursedOne:
					return "Cursed One";
				case CreatureClass.Hybrid:
					return "Hybrid";
					case CreatureClass.Insectoid:
					return "Insectoid";
				case CreatureClass.Elementa:
					return "Elementa";
				case CreatureClass.Relict:
					return "Relict";
				case CreatureClass.Ogroid:
					return "Ogroid";
				case CreatureClass.Draconid:
					return "Draconid";
				case CreatureClass.Vampire:
					return "Vampire";
				default:
					return "Unknown creature class";
			}
		} 
	}
	
	Application.Current().RegisterFilter('CreatureClassDisplay', new CreatureClassDisplayFilter());
}