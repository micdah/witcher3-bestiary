/// <reference path="../model/Sign.ts"/>

module witcher3bestiary {
	'use strict';
	
	export class Signs {
		public static Aard: Sign	= new Sign("Aard");
		public static Igni: Sign	= new Sign("Igni");
		public static Yrden: Sign	= new Sign("Yrden");
		public static Quen: Sign	= new Sign("Quen");
		public static Axii: Sign	= new Sign("Axii");
	}
}