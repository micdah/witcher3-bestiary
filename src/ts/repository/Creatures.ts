/// <reference path="../model/Creature.ts"/>
/// <reference path="../model/creatures/Necrophage.ts"/>
/// <reference path="../model/creatures/Specter.ts"/>
/// <reference path="../model/creatures/Beast.ts"/>
/// <reference path="../model/creatures/CursedOne.ts"/>
/// <reference path="../model/creatures/Hybrid.ts"/>
/// <reference path="../model/creatures/Insectoid.ts"/>
/// <reference path="../model/creatures/Elementa.ts"/>
/// <reference path="../model/creatures/Relict.ts"/>
/// <reference path="../model/creatures/Ogroid.ts"/>
/// <reference path="../model/creatures/Draconid.ts"/>
/// <reference path="../model/creatures/Vampire.ts"/>
/// <reference path="../repository/Bombs.ts"/>
/// <reference path="../repository/Oils.ts"/>
/// <reference path="../repository/Potions.ts"/>
/// <reference path="../repository/Signs.ts"/>

module witcher3bestiary {
	'use strict';
	
	export class Creatures {
		public static All: Array<Creature> = [
			// NECROPHAGES
			new Necrophage("Abaya",			null,				[]).Boss(),
			new Necrophage("Graveir",		"graveir",		[Oils.NecrophageOil]),
			new Necrophage("Alghoul",		"alghoul",		[Oils.NecrophageOil, Signs.Axii]),
			new Necrophage("Grave Hag",		"grave_hag",	[Oils.NecrophageOil, Potions.BlackBlood, Signs.Yrden, Signs.Quen]),
			new Necrophage("Bilge Hag",		"bilge_hag",	[Oils.NecrophageOil, Bombs.NorthernWind, Signs.Quen, Signs.Igni]),
			new Necrophage("Ignis Fatuus",	"ignis_fatuus",	[Oils.NecrophageOil, Bombs.MoonDust, Signs.Quen]).Boss(),
			new Necrophage("Devourer",		"devourer",		[Oils.NecrophageOil]),
			new Necrophage("Mourntart",		"mourntart",	[Oils.NecrophageOil, Potions.BlackBlood, Signs.Yrden, Signs.Quen]).Boss(),
			new Necrophage("Drowned Dead",	"drowned_dead",	[Oils.NecrophageOil, Signs.Igni]),
			new Necrophage("Mucknixer",		"mucknixer",	[Oils.NecrophageOil, Signs.Igni]),
			new Necrophage("Drowner",		"drowner",		[Oils.NecrophageOil, Signs.Igni]),
			new Necrophage("Rotfiend",		"rotfiend",		[Oils.NecrophageOil]),
			new Necrophage("Foglet",		"foglet",		[Oils.NecrophageOil, Bombs.MoonDust, Signs.Quen]),
			new Necrophage("Tangalore",		"tangalore",	[Oils.NecrophageOil, Bombs.MoonDust, Signs.Quen]),
			new Necrophage("Ghoul",			"ghoul",		[Oils.NecrophageOil]),
			new Necrophage("Vodnick",		"vodnick",		[Oils.NecrophageOil, Signs.Igni]),
			new Necrophage("Water Hag",		"water_hag",	[Oils.NecrophageOil, Bombs.NorthernWind, Signs.Igni, Signs.Quen]),
			
			// SPECTERS
			new Specter("Hym",					"hym",					[Oils.SpecterOil, Bombs.MoonDust, Signs.Igni]),
			new Specter("Jenny O' the Woods",	"jenny_o_the_woods",	[Oils.SpecterOil, Bombs.MoonDust, Signs.Yrden]).Boss(),
			new Specter("Nightwraith",			"nightwraith",			[Oils.SpecterOil, Bombs.MoonDust, Signs.Yrden]),
			new Specter("Noonwraith",			"noonwraith",			[Oils.SpecterOil, Bombs.MoonDust, Signs.Yrden]),
			new Specter("Penitent",				"penitent",				[Oils.SpecterOil, Bombs.MoonDust, Signs.Yrden, Signs.Quen]),
			new Specter("Plague Maiden",		"plague_maiden",		[Oils.SpecterOil, Signs.Yrden]),
			new Specter("White Lady",			"white_lady",			[Oils.SpecterOil, Bombs.DimeritiumBomb, Bombs.MoonDust, Signs.Yrden]),
			new Specter("Wraith",				"wraith",				[Oils.SpecterOil, Bombs.MoonDust, Signs.Yrden, Signs.Quen]),
			
			// BEASTS
			new Beast("Bear",		"bear",			[Oils.BeastOil, Signs.Quen, Signs.Igni]),
			new Beast("Dog",		"dog",			[Oils.BeastOil, Signs.Quen, Signs.Igni]),
			new Beast("White Wolf",	"white_wolf",	[Oils.BeastOil, Signs.Quen, Signs.Igni]),
			new Beast("Wild Dog",	"wild_dog",		[Oils.BeastOil, Signs.Quen, Signs.Igni]),
			new Beast("Wolf",		"wolf",			[Oils.BeastOil, Signs.Quen, Signs.Igni]),
			
			// CURSED ONES
			new CursedOne("Berserker",	"berserker",	[Oils.CursedOil, Bombs.DevilsPuffbal, Signs.Quen, Signs.Igni]),
			new CursedOne("Botchling",	"botchling",	[Oils.CursedOil, Signs.Axii]),
			new CursedOne("Ulfhedinn",	"ulfhedinn",	[Oils.CursedOil, Bombs.MoonDust, Bombs.DevilsPuffbal, Signs.Igni]),
			new CursedOne("Werewolf",	"werewolf",		[Oils.CursedOil, Bombs.MoonDust, Bombs.DevilsPuffbal, Signs.Igni]),
			
			// HYBRIDS
			new Hybrid("Archgriffin",	"archgriffin",		[Oils.HybridOil, Bombs.Grapeshot, Signs.Aard]),
			new Hybrid("Harpy",			"harpy",			[Oils.HybridOil, Bombs.Grapeshot, Signs.Aard]),
			new Hybrid("Ekhidna",		"ekhidna",			[Oils.HybridOil, Bombs.Grapeshot, Signs.Aard, Signs.Igni]),
			new Hybrid("Erynia",		"erynia",			[Oils.HybridOil, Bombs.Grapeshot, Signs.Aard]),
			new Hybrid("Griffin",		"griffin",			[Oils.HybridOil, Bombs.Grapeshot, Signs.Aard]),
			new Hybrid("Lamia",			"lamia",			[Oils.HybridOil, Bombs.Grapeshot, Signs.Aard, Signs.Igni]),
			new Hybrid("Melusine",		"melusine",			[Oils.HybridOil, Bombs.Grapeshot, Signs.Aard, Signs.Igni]),
			new Hybrid("Royal Griffin",	"royal_griffin",	[Signs.Igni]).Boss(),
			new Hybrid("Salma",			"salma",			[Oils.HybridOil, Signs.Quen]),
			new Hybrid("Siren",			"siren",			[Oils.HybridOil, Bombs.Grapeshot, Signs.Aard, Signs.Igni]),
			new Hybrid("Succubus",		"succubus",			[Oils.HybridOil, Signs.Quen]),
			
			// INSECTOIDS
			new Insectoid("Arachas",			"arachas",			[Oils.InsectoidOil, Potions.GoldenOriole]),
			new Insectoid("Armored Arachas",	"armored_arachas",	[Oils.InsectoidOil, Potions.GoldenOriole]),
			new Insectoid("Endrega Drone",		"endrega_drone",	[Oils.InsectoidOil, Potions.GoldenOriole]),
			new Insectoid("Endrega Warrior",	"endrega_warrior",	[Oils.InsectoidOil, Potions.GoldenOriole]),
			new Insectoid("Endrega Worker",		"endrega_worker",	[Oils.InsectoidOil, Potions.GoldenOriole]),
			new Insectoid("Venomous Arachas",	"venomous_arachas",	[Oils.InsectoidOil, Potions.GoldenOriole]),
			
			// ELEMENTA
			new Elementa("D'ao",					"d_ao",						[Oils.ElementaOil, Bombs.DimeritiumBomb]).Boss(),
			new Elementa("Djinn",					"djinn",					[Oils.ElementaOil, Bombs.DimeritiumBomb]),
			new Elementa("Fire Elemental",			"fire_elemental",			[Oils.ElementaOil, Bombs.DimeritiumBomb, Bombs.NorthernWind, Signs.Aard]),
			new Elementa("Gargoyle",				"gargoyle",					[Oils.ElementaOil, Bombs.DimeritiumBomb, Signs.Quen]),
			new Elementa("Golem",					"golem",					[Oils.ElementaOil, Bombs.DimeritiumBomb]),
			new Elementa("Hound of the Wild Hunt",	"hound_of_the_wild_hunt",	[Oils.ElementaOil, Bombs.DimeritiumBomb, Signs.Igni, Signs.Axii]),
			new Elementa("Ice Elemental",			"ice_elemental",			[Oils.ElementaOil, Bombs.DimeritiumBomb]),
			new Elementa("Ifirt",					"ifrit",					[Oils.ElementaOil, Bombs.DimeritiumBomb, Bombs.NorthernWind, Signs.Aard]).Boss(),
			new Elementa("Therazane",				"therazane",				[Oils.ElementaOil, Bombs.DimeritiumBomb]).Boss(),
			new Elementa("The Apiarian Phantom",	"the_apiarian_phantom",		[Oils.ElementaOil, Bombs.DimeritiumBomb, Signs.Igni, Signs.Axii]).Boss(),
			
			// RELICTS
			new Relict("Chort",		"chort",	[Oils.RelictOil, Bombs.DevilsPuffbal]),
			new Relict("Doppler",	"doppler",	[Oils.RelictOil]),
			new Relict("Fiend",		"fiend",	[Oils.RelictOil, Bombs.DevilsPuffbal, Bombs.Samum]),
			new Relict("Fugas",		null,			[Oils.RelictOil, Bombs.DevilsPuffbal, Bombs.Samum, Signs.Igni]),
			new Relict("Howler",	"howler",	[Oils.RelictOil, Bombs.DevilsPuffbal]),
			new Relict("Imp",		"imp",		[Oils.RelictOil]),
			new Relict("Kernun",	"kernun",	[Oils.RelictOil, Bombs.DimeritiumBomb, Signs.Igni]),
			new Relict("Leshen",	"leshen",	[Oils.RelictOil, Bombs.DimeritiumBomb, Signs.Igni]),
			new Relict("Morvudd",	"morvudd",	[Oils.RelictOil, Bombs.DevilsPuffbal, Bombs.Samum]),
			new Relict("Sylvan",	"sylvan",	[Oils.RelictOil, Bombs.DevilsPuffbal, Bombs.Samum, Signs.Igni]),
			
			// OGROIDS
			new Ogroid("Cave Troll",		"cave_troll",		[Oils.OgroidOil, Signs.Quen]),
			new Ogroid("Cyclops",			"cyclops",			[Oils.OgroidOil, Signs.Axii, Signs.Quen]),
			new Ogroid("Hagubman",			"hagubman",			[Oils.OgroidOil, Bombs.NorthernWind]),
			new Ogroid("Ice Giant",			"ice_giant",		[Oils.OgroidOil, Signs.Quen]),
			new Ogroid("Ice Troll",			"ice_troll",		[Oils.OgroidOil, Signs.Quen]),
			new Ogroid("Nekker",			"nekker",			[Oils.OgroidOil, Bombs.NorthernWind]),
			new Ogroid("Nekker Warrior",	"nekker_warrior",	[Oils.OgroidOil, Bombs.NorthernWind]),
			new Ogroid("Rock Troll",		"rock_troll",		[Oils.OgroidOil, Signs.Quen]),
			new Ogroid("Speartip",			"speartip",			[Oils.OgroidOil, Signs.Axii, Signs.Quen]),
			
			// DRACONIDS
			new Draconid("Basilisk",		"basilisk",		[Oils.DraconidOil, Potions.GoldenOriole, Bombs.Grapeshot, Signs.Aard]),
			new Draconid("Cockatrice",		"cockatrice",	[Oils.DraconidOil, Bombs.Grapeshot, Signs.Aard]),
			new Draconid("Forktail",		"forktail",		[Oils.DraconidOil, Potions.GoldenOriole, Bombs.Grapeshot, Signs.Aard]),
			new Draconid("Shrieker",		"shrieker",		[Oils.DraconidOil, Signs.Aard]),
			new Draconid("Royal Wyvern",	"royal_wyvern",	[Oils.DraconidOil, Potions.GoldenOriole, Bombs.Grapeshot, Signs.Aard]),
			new Draconid("Wyvern",			"wyvern",		[Oils.DraconidOil, Potions.GoldenOriole, Bombs.Grapeshot, Signs.Aard]),
			
			// VAMPIRES
			new Vampire("Higher Vampire",	"higher_vampire",	[Oils.VampireOil, Signs.Igni]),
			new Vampire("Gael",				"gael",				[Oils.VampireOil, Bombs.MoonDust, Bombs.DevilsPuffbal, Signs.Yrden, Signs.Igni]),
			new Vampire("Katakan",			"katakan",			[Oils.VampireOil, Bombs.MoonDust, Bombs.DevilsPuffbal, Signs.Yrden, Signs.Igni]),
			new Vampire("Sarasti",			"sarasti",			[Oils.VampireOil, Bombs.DevilsPuffbal, Signs.Igni]),
			new Vampire("Vampire",			null,					[Oils.VampireOil, Signs.Igni]),
		];
	}	
}