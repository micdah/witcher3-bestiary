/// <reference path="../model/Oil.ts"/>

module witcher3bestiary {
	'use strict';
	
	export class Oils {
		public static NecrophageOil: Oil	= new Oil("Necrophage Oil",	"necrophage");
		public static SpecterOil: Oil		= new Oil("Specter Oil", "specter");
		public static BeastOil: Oil			= new Oil("Beast Oil", "beast");
		public static CursedOil: Oil		= new Oil("Cursed Oil", "cursed");
		public static HybridOil: Oil		= new Oil("Hybrid Oil", "hybrid");
		public static InsectoidOil: Oil		= new Oil("Insectoid Oil", "insectoid");
		public static ElementaOil: Oil		= new Oil("Elementa Oil", "elementa");
		public static RelictOil: Oil		= new Oil("Relict Oil", "relict");
		public static OgroidOil: Oil		= new Oil("Ogroid Oil", "ogroid");
		public static DraconidOil: Oil		= new Oil("Draconid Oil", "draconid");
		public static VampireOil: Oil		= new Oil("Vampire Oil", "vampire");
	}
}