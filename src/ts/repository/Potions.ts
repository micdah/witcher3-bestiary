/// <reference path="../model/Potion.ts"/>

module witcher3bestiary {
	'use strict';
	
	export class Potions {
		public static BlackBlood: Potion	= new Potion("Black Blood", "black_blood");
		public static GoldenOriole: Potion	= new Potion("Golden Oriole", "golden_oriole");
	}
}