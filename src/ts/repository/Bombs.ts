/// <reference path="../model/Bomb.ts"/>

module witcher3bestiary {
	'use strict';
	
	export class Bombs {
		public static NorthernWind: Item	= new Bomb("Northern Wind", "northern_wind");
		public static MoonDust: Bomb		= new Bomb("Moon Dust", "moon_dust");
		public static DimeritiumBomb: Bomb	= new Bomb("Dimeritium Bomb", "dimeritium_bomb");
		public static DevilsPuffbal: Bomb	= new Bomb("Devil's Puffball", "devils_puffball");
		public static Grapeshot: Bomb		= new Bomb("Grapheshot", "grapeshot");
		public static Samum: Bomb			= new Bomb("Samum", "samum");
	}
}