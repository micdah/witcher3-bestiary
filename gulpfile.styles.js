var gulp		= require('gulp');
var less		= require('gulp-less');
var minifyCss	= require('gulp-minify-css');
var concat		= require('gulp-concat');
var del			= require('del');

var cssSrc = 'src/less/**/*.less';

gulp.task('styles-clean', function(cb) {
	del(['build/css/'], cb);
})

gulp.task('less-compile', ['styles-clean'], function() {
	return gulp.src(cssSrc, {'base': 'src/less/'})
		.pipe(less())
		.pipe(gulp.dest('build/css/'));
});

gulp.task('css-minify', ['styles-clean', 'less-compile', 'img-sprite'], function() {
	return gulp.src(['build/css/**/*.css', '!build/css/all.min.css'], {'base': 'build/css/'})
		.pipe(minifyCss({compatibility: 'ie8'}))
		.pipe(concat('all.min.css'))
		.pipe(gulp.dest('build/css'));
});

gulp.task('styles-build', ['styles-clean', 'less-compile', 'css-minify']);

gulp.task('styles-watch', ['styles-build'], function() {
	gulp.watch(cssSrc, ['styles-build']);
});