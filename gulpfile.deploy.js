var gulp	= require('gulp');
var del		= require('del');

var deployPath = '../micdah.bitbucket.org/witcher3/';

gulp.task('deploy-clean', function(cb) {
	del([deployPath], {force:true}, cb);
});


gulp.task('deploy', ['deploy-clean', 'build'], function() {
	return gulp.src('build/**/*', {'base': 'build/'})
		.pipe(gulp.dest(deployPath));
});